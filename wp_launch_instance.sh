#!/bin/bash

INSTANCE_NAME=$1

if (( $# < 1 )) 
then
   echo "Please specify webserver name"
   exit 1
fi

SG='sg-0ada7419dc3298d47'

TXT_FILE_SUFFIX='-details.txt'
THIRTY_TWO='/32'

# Launch the Webserver 
aws ec2 run-instances \
 --image-id ami-0ffea00000f287d30 \
 --count 1 \
 --instance-type t2.micro \
 --key-name TRiMS-Jenkins-Key \
 --security-group-ids $SG \
 --subnet-id subnet-7bddfc1d \
 --region eu-west-1 \
 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value='$INSTANCE_NAME'}]' >/dev/null

sleep 30

INSTANCE_ID=`aws --region eu-west-1 ec2 describe-instances \
 --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
 --query 'Reservations[*].Instances[*].[InstanceId]' \
 --output text`

# PUBLIC_IP=`aws --region eu-west-1 ec2 describe-instances \
#  --instance-ids $INSTANCE_ID \
#  --query 'Reservations[*].Instances[*].[PublicIpAddress]' \
#  --output text`

## Scraping details and outputting them into txt file
aws --region eu-west-1 ec2 describe-instances \
 --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
 --query 'Reservations[*].Instances[*].[InstanceId]' \
 --output text >$INSTANCE_NAME$TXT_FILE_SUFFIX

aws --region eu-west-1 ec2 describe-instances \
 --instance-ids $INSTANCE_ID \
 --query 'Reservations[*].Instances[*].[PublicIpAddress]' \
 --output text >>$INSTANCE_NAME$TXT_FILE_SUFFIX

aws --region eu-west-1 ec2 describe-instances \
 --instance-ids $INSTANCE_ID \
 --query 'Reservations[*].Instances[*].[PrivateIpAddress]' \
 --output text >>$INSTANCE_NAME$TXT_FILE_SUFFIX




