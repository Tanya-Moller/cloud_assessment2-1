#!/bin/bash


#export AWS_PROFILE=Academy 

### Variables

## Instance Name 
# INSTANCE_NAME='trims-pc-webserver'
# INSTANCE_NAME='trims-pc-bastion'
# INSTANCE_NAME='trims-pc-loadbalancer'

INSTANCE_NAME=$1

if (( $# < 1 )) 
then
   echo "Please specify webserver name"
   exit 1
fi

## Security Groups You want loadbalancer to have (DON'T CHANGE)

#╩LOADBALANCER = 'sg-01990fd6abd558a93'
SG='sg-01990fd6abd558a93'

SG_WEBSERVER='sg-0610bae0e1c9b8d5f'

##╩Suffixes (DONT CHANGE)
TXT_FILE_SUFFIX='-details.txt'
THIRTY_TWO='/32'



# Launch the loadbalancer 
aws ec2 run-instances \
   --image-id ami-0ffea00000f287d30 \
   --count 1 \
   --instance-type t2.micro \
   --key-name TRiMS-Jenkins-Key \
   --security-group-ids $SG \
   --subnet-id subnet-7bddfc1d \
   --region eu-west-1 \
   --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value='$INSTANCE_NAME'}]' >/dev/null
   

INSTANCE_ID=`aws --region eu-west-1 ec2 describe-instances \
 --filters "Name=tag:Name,Values=$INSTANCE_NAME" "Name=instance-state-name,Values=pending" \
 --query 'Reservations[*].Instances[*].[InstanceId]' \
 --output text`
 
sleep 30

## Scraping details and outputting them into txt file
aws --region eu-west-1 ec2 describe-instances \
   --instance-ids $INSTANCE_ID \
   --query 'Reservations[*].Instances[*].[InstanceId]' \
   --output text >$INSTANCE_NAME$TXT_FILE_SUFFIX

aws --region eu-west-1 ec2 describe-instances \
   --instance-ids $INSTANCE_ID \
   --query 'Reservations[*].Instances[*].[PublicIpAddress]' \
   --output text >>$INSTANCE_NAME$TXT_FILE_SUFFIX

aws --region eu-west-1 ec2 describe-instances \
   --instance-ids $INSTANCE_ID \
   --query 'Reservations[*].Instances[*].[PrivateIpAddress]' \
   --output text >>$INSTANCE_NAME$TXT_FILE_SUFFIX



## Adding the loadbalancer to webservers SG


#PRIVATE_IP=`aws --region eu-west-1 ec2 describe-instances \
 #--instance-ids $INSTANCE_ID \
 #--query 'Reservations[*].Instances[*].[PrivateIpAddress]' \
 #--output text`

#echo $PRIVATE_IP

#aws --region eu-west-1 ec2 authorize-security-group-ingress \
 #  --group-id $SG_WEBSERVER \
  # --protocol tcp \
   #--port 8080 \
   #--cidr $PRIVATE_IP$THIRTY_TWO >/dev/null

exit 0