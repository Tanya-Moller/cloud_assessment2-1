# Manual Commands To Setup PetClinic on a Server

## Setting up Amazon Linux Server

- Have to install PHP using https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html
  - Latest version of `php7.2=latest` is already enabled from `amazon-linux-extras`
  - `sudo yum update -y`
  - `sudo amazon-linux-extras install -y php7.2`
    - install it just incase
  - `sudo yum install -y httpd` 
    - installing apache
  - `sudo systemctl start httpd`
    - start the apache webserver
  - `sudo systemctl enable httpd`
    - configure the Apache web server to start at each system boot

  - TO CHECK PHP INSTALLATION NOT NECESSARY 
    - `sudo sh -c 'echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php'`
      - creates a .php file in directory which we can address via `<ec2-publicip>/phpinfo.php` and this should show a page confirming php version
    - `sudo rm /var/www/html/phpinfo.ph`
      - but we delete this file ans this information shouldnt be seen

  - `sudo yum -y install git`
  - `git clone https://ramana11235@bitbucket.org/JangleFett/simple_academy_php_app.git`

  - `cd simple_academy_php_app`


  - `rm README.md README2.md Create\ a\ Simple\ CRUD\ Database\ App_\ Connecting\ to\ MySQL\ with\ PHP\ \(Part\ 1_\ Create\,\ Read\)\ –\ Tania\ Rascia.pdf`
    - remove unncessary files

  - `cd public`
  
  - `sudo cp -R * /var/www/html/`




## CREATING PETCLINIC DATABASE AND FRONTEND 
### MySQl Amazon RDS AND EC2 INSTANCE

trimsadmin
trimspwdpwd2


- create an amazon linux ec2 instance (this is gonna run pet clinic and talk to a rds database that you're also going to setup)
  - https://aws.amazon.com/premiumsupport/knowledge-center/rds-connect-ec2-bastion-host/
    - Yo just follow these steps, it works wtf
  - worry about automating security groups later

- `sudo yum -y install git`

- `git clone https://ramana11235@bitbucket.org/JangleFett/petclinic.git`

- `sudo amazon-linux-extras install -y mariadb10.5`

- INSTALL JDK 8 FOR MAVEN
  - download into git and scp from local machine? (later jenkins workspace)
    - `sudo curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O 'http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz'`
  - `sudo mkdir /usr/lib/jvm/`
  - `cd /usr/lib/jvm`
  - `sudo tar -xvzf ~/jdk-8u131-linux-x64.tar.gz`
  - `sudo sh -c 'echo PATH=$PATH:/usr/lib/jvm/jdk1.8.0_131/bin:/usr/lib/jvm/jdk1.8.0_131/db/bin:/usr/lib/jvm/jdk1.8.0_131/jre/bin >>/etc/environment'`
  - `sudo sh -c 'echo J2SDKDIR="/usr/lib/jvm/jdk1.8.0_131" >>/etc/environment'`
  - `sudo sh -c 'echo J2REDIR="/usr/lib/jvm/jdk1.8.0_131/jre" >>/etc/environment'`
  - `sudo sh -c 'echo JAVA_HOME="/usr/lib/jvm/jdk1.8.0_131" >>/etc/environment'`
  - `sudo sh -c 'echo DERBY_HOME="/usr/lib/jvm/jdk1.8.0_131/db" >>/etc/environment'`
  - `sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_131/bin/java" 0`
  - `sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_131/bin/javac" 0`
  - `sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_131/bin/java`
  - `sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_131/bin/javac`
  - `update-alternatives --list java`
  - `update-alternatives --list javac`




- TO INSTALL MAVEN TO COMPILE PET CLINIC PROJECT
  - `cd /opt`
  - `sudo wget https://downloads.apache.org/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz`
  - `sudo tar -xvzf apache-maven-3.8.1-bin.tar.gz`
  - `sudo sh -c 'echo M2_HOME="/opt/apache-maven-3.8.1" >>/etc/environment'`
  - `sudo sed -i "s,PATH=\$PATH,PATH=$PATH:/opt/apache-maven-3.8.1/bin," /etc/environment`
  - `sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.1/bin/mvn" 0`
  - `sudo update-alternatives --set mvn /opt/apache-maven-3.8.1/bin/mvn`
  - `sudo wget https://raw.github.com/dimaj/maven-bash-completion/master/bash_completion.bash --output-document /etc/bash_completion.d/mvn`


- RUN THE THING TO POPULATE DB
  - `cd ~/petclinic/`
  - `sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic,spring.datasource.url=jdbc:mysql://<ENTER RDS SERVER ENDPOINT HERE>/petclinic," ~/petclinic/src/main/resources/application.properties`
    - changing localhost in .properties to the dns name/ip of rds server (FOR AUTOMATION REPLACE THE NAME WITH $VARIABLENAME)
  - `sudo sed -i "s,spring.datasource.username=petclinic,spring.datasource.username=<ENTER RDS SERVER USERNAME HERE>," ~/petclinic/src/main/resources/application.properties`
<<<<<<< HEAD
  - `sudo sed -i "s,spring.datasource.password=petclinic,spring.datasource.password=<ENTER RDS SERVER PW HERE>," ~/petclinic/src/main/resources/application.properties`
=======
  - `sudo sed -i "s,spring.datasource.password=petclinic,spring.datasource.password=<ENTER RDS SERVER PW HERE," ~/petclinic/src/main/resources/application.properties`
>>>>>>> fc7e0f98ca184c668cc050ea89696fa3cde69903


  - `mvn -Dmaven.test.skip=true package`
  
  - `cd ~/petclinic/src/main/resources/db/mysql/`
  - `sudo chmod +x data.sql schema.sql`
  - `sudo systemctl start mariadb`
  - `sudo mysql -h <ENTER RDS SERVER ENDPOINT HERE> -P 3306 -u trimsadmin -p`
    - THE -h OPTION IS FOR THE ENDPOINT OF RDS SERVER
    - -P IS PORT NUMBER
    - -u IS THE USERNAME
    - -p PROMPTS FOR PASSWORD
      - for scripting purposes write a delayed scripty thing which puts the password after 5 second delay 
    - https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ConnectToInstance.html this is where i got this command from, might be useful for other stuff
  - `source /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql;`
  - `source /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql;`
  - `CREATE USER 'petclinic'@'%' IDENTIFIED BY 'petclinic';`
  - `GRANT ALL PRIVILEGES ON petclinic.* TO 'petclinic'@'%';`

- ALL THE ABOVE populates the rdb server 
- Below runs petclinic on :8080, do security and portforwarding later 

  - `cd ~/petclinic`
  - `java -jar target/*.jar`





