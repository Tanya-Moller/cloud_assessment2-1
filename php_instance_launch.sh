#!/bin/bash

### Variables

## Instance Name 
#INSTANCE_NAME='trims-php-webserver-jenkins-test'


INSTANCE_NAME=$1

if (( $# < 1 )) 
then
   echo "Please specify webserver name"
   exit 1
fi


## Security Groups You Want Instance To Have (DON'T CHANGE) (UNCOMMENT ONE IN USE)

# WEBSERVER = 'sg-0610bae0e1c9b8d5f'
SG='sg-07bf68e80ef9a91d6'

SG_DATABASE='sg-09abe2c516d1ab7a9'


##�Suffixes (DONT CHANGE)
TXT_FILE_SUFFIX='-details.txt'
THIRTY_TWO='/32'



# Launch the Webserver 
aws ec2 run-instances \
 --image-id ami-0ffea00000f287d30 \
 --count 1 \
 --instance-type t2.micro \
 --key-name TRiMS-Jenkins-Key \
 --security-group-ids $SG \
 --subnet-id subnet-7bddfc1d \
 --region eu-west-1 \
 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value='$INSTANCE_NAME'}]' >/dev/null

sleep 30

INSTANCE_ID=`aws --region eu-west-1 ec2 describe-instances \
 --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
 --query 'Reservations[*].Instances[*].[InstanceId]' \
 --output text`

# PUBLIC_IP=`aws --region eu-west-1 ec2 describe-instances \
#  --instance-ids $INSTANCE_ID \
#  --query 'Reservations[*].Instances[*].[PublicIpAddress]' \
#  --output text`

## Scraping details and outputting them into txt file
aws --region eu-west-1 ec2 describe-instances \
 --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
 --query 'Reservations[*].Instances[*].[InstanceId]' \
 --output text >$INSTANCE_NAME$TXT_FILE_SUFFIX

aws --region eu-west-1 ec2 describe-instances \
 --instance-ids $INSTANCE_ID \
 --query 'Reservations[*].Instances[*].[PublicIpAddress]' \
 --output text >>$INSTANCE_NAME$TXT_FILE_SUFFIX

aws --region eu-west-1 ec2 describe-instances \
 --instance-ids $INSTANCE_ID \
 --query 'Reservations[*].Instances[*].[PrivateIpAddress]' \
 --output text >>$INSTANCE_NAME$TXT_FILE_SUFFIX



# Add webserver ip to database security group on port 3306

PRIVATE_IP=`aws --region eu-west-1 ec2 describe-instances \
 --instance-ids $INSTANCE_ID \
 --query 'Reservations[*].Instances[*].[PrivateIpAddress]' \
 --output text`

echo $PRIVATE_IP
 
aws --region eu-west-1 ec2 authorize-security-group-ingress \
   --group-id $SG_DATABASE \
   --protocol tcp \
   --port 3306 \
   --cidr $PRIVATE_IP$THIRTY_TWO >/dev/null



# ./php_instance_launch.sh trims-php-webserver
# ./ec2_scrape_script.sh trims-php-webserver