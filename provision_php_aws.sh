#!/bin/bash

WEBSERVER_FILE=$1 ## FILL IN MANUALY FOR JENKINS PURPOSE
DB_FILE=$2
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.php'
#WEBSERVER_FILE='trims-php-webserver-jenkins-test' ## FILL IN MANUALY FOR JENKINS PURPOSE
#DB_FILE='trims-php-db-jenkins-test'


if [ -f ${WEBSERVER_FILE}${TXT_FILE_SUFFIX} ]
then
   echo "Starting webserver: $WEBSERVER_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$WEBSERVER_FILE instance does not exist."
   exit 1
fi

if [ -f ${DB_FILE}${CNF_FILE_SUFFIX} ]
then
   echo "Pulling Data From: $DB_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$DB_FILE instance does not exist."
   exit 1
fi


## saves the second line (by design it is the public ip) and assigns it to hostname as usual
hostname=$(sed -n '2p' ${WEBSERVER_FILE}${TXT_FILE_SUFFIX})


scp -o StrictHostKeyChecking=no $DB_FILE$CNF_FILE_SUFFIX ec2-user@$hostname:config.php
echo "SCP complete"
ssh -o StrictHostKeyChecking=no ec2-user@$hostname ' 

sudo yum update -y
sudo amazon-linux-extras install -y php7.2 lamp-mariadb10.2-php7.2

sudo yum -y install httpd
if rpm -qa | grep "^httpd-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start httpd
  sudo systemctl enable httpd
else
    exit 1
fi

sudo usermod -a -G apache ec2-user
sudo chown -R ec2-user:apache /var/www
sudo chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;

sudo yum -y install git

yes | git clone https://Tanya-Moller@bitbucket.org/Tanya-Moller/janglefett-simple_academy_php_app_.git

sudo cp -a janglefett-simple_academy_php_app_/. /var/www/html

rm -rf ~/janglefett-simple_academy_php_app_

cd /var/www/html

rm config.php

mv ~/config.php /var/www/html/

# sudo sed -i "s,$host       = getenv(\"DBHOST\");,$host       = \"trims-database-php.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com\";," config.php
# sudo sed -i "s,$username   = getenv(\"DBUSER\");,$username   = \"academy\";," config.php
# sudo sed -i "s,$password   = getenv(\"DBPASS\");,$password   = \"assessment2\";," config.php
# sudo sed -i "s,$dbname     = \"phpapp\";,$dbname     = \"PHPDatabase\";," config.php

php install.php
'