#!/bin/bash

## To run script, the argument must be the name of the instance
## e.g. the command looks like: $./pc_1_db_setup_install_dependencies.sh trims-pc-webserver

WEBSERVER_FILE=$1 ## FILL IN MANUALY FOR JENKINS PURPOSE
DB_FILE=$2
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.cnf'


if (( $# < 2 )) 
then
  echo "You must supply the Webserver Name AND Database Name (in that order)" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit  

fi

if [ -f ${WEBSERVER_FILE}${TXT_FILE_SUFFIX} ]
then
   echo "Starting webserver: $WEBSERVER_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$WEBSERVER_FILE instance does not exist."
   exit 1
fi

if [ -f ${DB_FILE}${CNF_FILE_SUFFIX} ]
then
   echo "Pulling Data From: $DB_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$DB_FILE instance does not exist."
   exit 1
fi


## saves the second line (by design it is the public ip) and assigns it to hostname as usual
hostname=$(sed -n '2p' ${WEBSERVER_FILE}${TXT_FILE_SUFFIX})

scp -o StrictHostKeyChecking=no petclinic.init ec2-user@$hostname:petclinic.init
scp -o StrictHostKeyChecking=no $DB_FILE$CNF_FILE_SUFFIX ec2-user@$hostname:/tmp/db_config.cnf
ssh -o StrictHostKeyChecking=no ec2-user@$hostname ' 

cd /tmp/
tail -n +2 db_config.cnf >>~/.bash_profile
source ~/.bash_profile


## Dependency Installation 
sudo yum install -y git

cd ~/
sudo git clone https://ramana11235@bitbucket.org/JangleFett/petclinic.git


sudo amazon-linux-extras install -y mariadb10.5


## Installing JDK 8
sudo curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz"

sudo mkdir /usr/lib/jvm/

cd /usr/lib/jvm

sudo tar -xvzf ~/jdk-8u131-linux-x64.tar.gz

sudo sh -c "echo PATH=$PATH:/usr/lib/jvm/jdk1.8.0_131/bin:/usr/lib/jvm/jdk1.8.0_131/db/bin:/usr/lib/jvm/jdk1.8.0_131/jre/bin >>/etc/environment"

sudo sh -c "echo J2SDKDIR=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo J2REDIR=\"/usr/lib/jvm/jdk1.8.0_131/jre\" >>/etc/environment"
sudo sh -c "echo JAVA_HOME=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo DERBY_HOME=\"/usr/lib/jvm/jdk1.8.0_131/db\" >>/etc/environment"

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_131/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_131/bin/javac" 0
sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_131/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_131/bin/javac
update-alternatives --list java
update-alternatives --list javac


## Installing Maven
cd /opt
sudo wget https://downloads.apache.org/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz
sudo tar -xvzf apache-maven-3.8.1-bin.tar.gz
sudo sh -c "echo M2_HOME=\"/opt/apache-maven-3.8.1\" >>/etc/environment"
sudo sh -c "echo PATH=$PATH:/opt/apache-maven-3.8.1/bin >>/etc/environment"
sudo sed -i "s,PATH=\$PATH,PATH=$PATH:/opt/apache-maven-3.8.1/bin," /etc/environment
sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.1/bin/mvn" 0
sudo update-alternatives --set mvn /opt/apache-maven-3.8.1/bin/mvn
sudo wget https://raw.github.com/dimaj/maven-bash-completion/master/bash_completion.bash --output-document /etc/bash_completion.d/mvn


## Git Cloning petclininc app
#cd ~/
#sudo git clone https://ramana11235@bitbucket.org/JangleFett/petclinic.git

cd ~/petclinic/

sudo mv ~/petclinic.init /etc/init.d/petclinic

sudo chmod +x /etc/init.d/petclinic

sudo chkconfig --add petclinic
#sudo systemctl enable petclinic

cd ~/petclinic
sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic,spring.datasource.url=jdbc:mysql://$host/petclinic," ~/petclinic/src/main/resources/application.properties
sudo sed -i "s,spring.datasource.username=petclinic,spring.datasource.username=$user," ~/petclinic/src/main/resources/application.properties
sudo sed -i "s,spring.datasource.password=petclinic,spring.datasource.password=$password," ~/petclinic/src/main/resources/application.properties


'

