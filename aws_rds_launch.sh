#!/bin/bash


### Variables

## Instance Name 

INSTANCE_NAME=$1
DB_USERNAME='trimsadmin'
DB_PASSWORD='trimspwdpwd123'


if (( $# < 1 )) 
then
   echo "Please specify webserver name"
   exit 1
fi

## Security Groups You Want To Instance To Have (DON'T CHANGE) 

# DATABASE_SG = 'sg-0b59185681cce1214'
SG='sg-0b59185681cce1214'


##�Suffixes (DON'T CHANGE)
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.cnf'


## Creating RDS DB Instance
aws rds create-db-instance \
    --db-instance-identifier $INSTANCE_NAME \
    --db-instance-class db.t2.micro \
    --engine mysql \
    --master-username $DB_USERNAME \
    --master-user-password $DB_PASSWORD\
    --vpc-security-group-ids $SG \
    --region eu-west-1 \
    --availability-zone eu-west-1a \
    --allocated-storage 20 >/dev/null

sleep 270  ## give time to let endpoint address to generate

## Creating .cnf file
echo "[client]" >$INSTANCE_NAME$CNF_FILE_SUFFIX
echo user=$DB_USERNAME >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo password=$DB_PASSWORD >>$INSTANCE_NAME$CNF_FILE_SUFFIX

# echo host=$(sed -n '1p' $INSTANCE_NAME$TXT_FILE_SUFFIX) >>$INSTANCE_NAME$CNF_FILE_SUFFIX

echo host=$(aws --region eu-west-1 rds describe-db-instances \
 --db-instance-identifier $INSTANCE_NAME \
 --query 'DBInstances[*].{host:Endpoint.Address}' \
 --output text) >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "port=3306" >>$INSTANCE_NAME$CNF_FILE_SUFFIX

# echo "database=petclinic" >>$INSTANCE_NAME$CNF_FILE_SUFFIX  ##might need this for later, keep for now


exit 0

