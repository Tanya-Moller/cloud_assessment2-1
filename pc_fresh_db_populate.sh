#!/bin/bash


## To run script, the argument must be the name of the bastion and the DB name
## e.g. the command looks like: $./pc_2_initial_db_population_thru_bastion.sh trims-pc-bastion trims-pc-db

INSTANCE_NAME=$1
DB_FILE=$2
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.cnf'

if (( $# < 2 )) 
then
  echo "You must supply the Main Webserver Name AND DB Name (in that order)" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit  

fi

if [ -f $INSTANCE_NAME$TXT_FILE_SUFFIX ]
then
   echo "Starting $INSTANCE_NAME changes."
else
  ## searches for the automatically created file which has instance name in the file
   echo "$INSTANCE_NAME instance does not exist."
   exit 1
fi

if [ -f $DB_FILE$CNF_FILE_SUFFIX ]
then
   echo "Starting $DB_FILE changes."
else
  ## searches for the automatically created file which has instance name in the file
   echo "$DB_FILE instance does not exist."
   exit 1
fi

## saves the second line (by design it is the public ip) and assigns it to hostname as usual
hostname=$(sed -n '2p' $INSTANCE_NAME$TXT_FILE_SUFFIX)


scp -o StrictHostKeyChecking=no $DB_FILE$CNF_FILE_SUFFIX ec2-user@$hostname:/tmp/db_config.cnf
ssh -o StrictHostKeyChecking=no ec2-user@$hostname ' 


cd /tmp/
tail -n +2 db_config.cnf >>~/.bash_profile
source ~/.bash_profile



## To Populate DB on RDS DB Server

cd ~/petclinic/

#sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic,spring.datasource.url=jdbc:mysql://$host/petclinic," ~/petclinic/src/main/resources/application.properties
#sudo sed -i "s,spring.datasource.username=petclinic,spring.datasource.username=$user," ~/petclinic/src/main/resources/application.properties
#sudo sed -i "s,spring.datasource.password=petclinic,spring.datasource.password=$password," ~/petclinic/src/main/resources/application.properties

sudo mvn -Dmaven.test.skip=true package

cd ~/petclinic/src/main/resources/db/mysql/

sudo chmod +x data.sql schema.sql

sudo systemctl start mariadb

sudo mysql --defaults-file=/tmp/db_config.cnf --execute="source /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql;"
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="source /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql;"
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="CREATE USER \"petclinic\"@\"%\" IDENTIFIED BY \"petclinic\";" --database=petclinic
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="GRANT ALL PRIVILEGES ON petclinic.* TO \"petclinic\"@\"%\";" --database=petclinic

exit 0
'

