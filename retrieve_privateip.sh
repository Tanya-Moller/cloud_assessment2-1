#!/bin/bash 

export AWS_PROFILE=Academy 

# Retrieve Instance id using tag name 
Instance_ID=`aws --region eu-west-1 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag-key,Values=MoPetclinic" --query 'Reservations[*].Instances[*].[InstanceId]' --output text` 

# Retrieve private ip using instance id 
Private_IP=`aws --region eu-west-1 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=instance-id,Values=$Instance_ID" --query 'Reservations[*].Instances[*].[PrivateIpAddress]' | grep -vE '\[|\]' | awk -F'"' '{ print $2 }'`

a='/32'
echo $a

echo "Mo_PetClinic_Info"
echo "Instance_ID="$Instance_ID
echo "Private_IP="$Private_IP$a 
echo "///////////////"

aws --region eu-west-1 ec2 authorize-security-group-ingress --group-name sc-trims-pc-db --protocol tcp --port 3306 --cidr $Private_IP$a



# Find a way upon creation of instance to provide instance with a unique tag probably through a bash variable
# In webserver_script.sh 
# Name='example'
# Key='MoPetclinic'
# Add $Name and $Key to the run_instances command