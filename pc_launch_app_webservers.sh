#!/bin/bash

WEBSERVER_FILE=$1 ## FILL IN MANUALY FOR JENKINS PURPOSE
DB_FILE=$2
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.cnf'



if (( $# < 1 )) 
then
  echo "You must supply the Webserver Name" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit  

fi

if [ -f $WEBSERVER_FILE$TXT_FILE_SUFFIX ]
then
   echo "Starting webserver: $WEBSERVER_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$WEBSERVER_FILE instance does not exist."
   exit 1
fi


## saves the second line (by design it is the public ip) and assigns it to hostname as usual
hostname=$(sed -n '2p' ${WEBSERVER_FILE}${TXT_FILE_SUFFIX})

ssh -o StrictHostKeyChecking=no ec2-user@$hostname ' 



## To Initalise and Start PC Webapp

cd ~/petclinic/

sudo chkconfig --add petclinic

sudo /etc/init.d/petclinic stop >/dev/null 2>&1 
sudo /etc/init.d/petclinic start >/dev/null 2>&1 

#sudo mvn -f /home/ec2-user/petclinic/ -Dmaven.test.skip=true package
#sudo java -jar /home/ec2-user/petclinic/target/*.jar >/dev/null 2>&1 &

sleep 60

exit 0

'

