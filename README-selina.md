# Cloud Assessment 2

## Amazon Linux - RDS & Database (MYSQL)

### Set up EC2 Machine 

1. Set up as you normally would
2. Once launched, Choose Next: Configure Security Group, choose Add Rule, and enter the following:

    Type: Custom TCP Rule<br/>
    Protocol: TCP<br/>
    Port Range: 22<br/>
    Source: Enter the **IP address** of your local machine.

3. ssh into ec2 instance 
   
   ```
   $ ssh -i ~/.ssh/<yourKey.pem> ec2-user@<IP Hostname>

   ```
4. Install MariaDB
   
   ```
   $ sudo yum update -y

   $ sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2

   $ sudo systemctl start mariadb

   $ sudo mysql_secure_installation
   > Prompts to enter password

   > 4 options will come up say Y to all of them:
   - Remove anonymous users? [Y/n]
   - Disallow root login remotely? [Y/n]
   - Remove test database and access to it? [Y/n]
   - Reload privilege tables now? [Y/n]
   
   $ sudo systemctl enable mariadb
   ```

5. You can access MariaDB
   
   ```
   $ sudo mysql -u root -p
   > Prompts passsword
   ```
   You can create and edit DB's in terminal using SQL syntax

   ```Example 
   $ show databases;
   $ DROP DATABASE IF EXISTS db_name;
   $ CREATE DATABASE db_name;
   ```
6. Connect Ec2 to MYSQL Workbench on computer
   
   ```
   $ sudo mysql -h <RDS Endpoint> -P <RDS PORT> -u admin -p

   > Prompts to enter password of RDS DB

   ```
7. Connect Amazon linux server (PHP installed) to Database (MYSQL) RDS 
   
   - Add rule to RDS DB security group
   - Enter the following:
 
    Type: Custom TCP Rule<br/>
    Protocol: TCP<br/>
    Port Range: Enter the port of your RDS DB instance (3306)<br/>
    Source: Enter the **Public IP address** of the EC2 instance with PHP installed<br/>


### Set up RDS DB instance

https://aws.amazon.com/getting-started/hands-on/create-mysql-db/

1. Once RDS instance is set up, click VPC security group
2. Go to actions and Edit inbound rules
3. Choose Add rule and enter the following:
 
    Type: Custom TCP Rule<br/>
    Protocol: TCP<br/>
    Port Range: Enter the port of your RDS DB instance (3306)<br/>
    Source: Enter the **Private IP address** of your EC2 instance<br/>

### Connect to the RDS DB instance from your computer (MYSQL workbench)

1. Start a new connection, and select Standard TCP/IP over SSH for the Connection Method.
2. Enter the following details about the EC2 instance for the SSH settings:

    **SSH Hostname:** Enter the public DNS name of the EC2 instance.<br/>
    **SSH Username:** Enter the user name for your EC2 instance. For example, "ec2-user" is the user name for EC2 Linux machines.<br/>
    **SSH Key File:** Select the private key that was used when the EC2 instance was created.

3. Enter the following details for the MySQL instance settings:

    **MySQL Hostname:** Enter the RDS DB instance endpoint<br/>
    **MySQL Server port:** Enter 3306 (or the custom port that you use).<br/>
    **Username:** Enter the master user name of the RDS DB instance.
    
4. Choose Test Connection.
   - Prompts you to Enter password for RDS DB instance.

### Connecting MYSQL to PHP server 

```
 $ sudo mysql -h <RDS Endpoint> -P <RDS PORT> -u admin -p

   > Prompts to enter password of RDS DB
```
change host etc in php.chkconfig file
   
## Ubuntu - NGINX & Wildfly install

### NGINX Installation 

On ubuntu_server1.ph provision 

```Terminal install of Nginx
$ ssh -i ~/.ssh/SelinaLyKey.pem ubuntu@<IPHostname>

$ sudo apt update
$ sudo apt install nginx
```
Check if Nginx is working and allow ports:

```C
$ sudo ufw app list
$ sudo ufw allow 'Nginx HTTP'
$ sudo ufw status
$ sudo ufw default allow
$ sudo ufw enable
$ sudo ufw status
```
### Installation of Wildfly

https://linuxize.com/post/how-to-install-wildfly-on-ubuntu-18-04/ 

1. Install the OpenJDK package by running:
   
   ```
   $ sudo apt install default-jdk
   ```

2. Create User
   - create new system user and group called wildfly with home directory **/opt/wildfly** that will run the WildFly service:
    
     ```
     $ sudo groupadd -r wildfly
     
     $ sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly
     ```

3. Install Wildfly
   
   ```
   $ WILDFLY_VERSION=16.0.0.Final

   $ wget https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz -P /tmp
   ```
   - Once the download is completed, extract the tar.gz file and move it to the /opt directory:
  
   ```
   $ sudo tar xf /tmp/wildfly-$WILDFLY_VERSION.tar.gz -C /opt/
   ```

4. Adjust the Firewall

   ```
   $ sudo ufw allow 8080/tcp
   ```
