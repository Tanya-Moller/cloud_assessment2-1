#!/bin/bash

### Variables

## Instance Name 

#INSTANCE_NAME='trimsjenkinstest'
INSTANCE_NAME=$1
DB_USERNAME='academy'
DB_PASSWORD='assessment2'

if (( $# < 1 )) 
then
  echo "Please specify webserver name"
  exit 1
fi

## Security Groups You Want To Instance To Have (DON'T CHANGE) 

# My PHP database
SG='sg-09abe2c516d1ab7a9' 


##Â Suffixes (DON'T CHANGE)
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.php'


## Creating RDS DB Instance
aws rds create-db-instance \
     --db-instance-identifier $INSTANCE_NAME \
     --db-instance-class db.t2.micro \
     --engine mysql \
     --master-username $DB_USERNAME \
     --master-user-password $DB_PASSWORD\
     --vpc-security-group-ids $SG \
     --region eu-west-1 \
     --availability-zone eu-west-1a \
     --allocated-storage 20 >/dev/null

 sleep 250  ## give time to let endpoint address to generate

INSTANCE_ENDPOINT=`aws --region eu-west-1 rds describe-db-instances \
 --db-instance-identifier $INSTANCE_NAME \
 --query 'DBInstances[*].{host:Endpoint.Address}' \
 --output text`

## Creating .php file

echo "<?php" >$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "/**" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo " * Configuration for database connection" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo " *" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo " */" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "\$host       = \"$INSTANCE_ENDPOINT\";" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "\$username   = \"$DB_USERNAME\";" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "\$password   = \"$DB_PASSWORD\";" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "\$dbname     = \"PHPDatabase\";" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "\$dsn        = \"mysql:host=\$host;dbname=\$dbname\";" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "\$options    = array(" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION" >>$INSTANCE_NAME$CNF_FILE_SUFFIX
echo "              );" >>$INSTANCE_NAME$CNF_FILE_SUFFIX


exit 0
